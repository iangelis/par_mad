import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


import scienceplots

# plt.rcParams["backed"] =
plt.style.use(["notebook", "science"])
plt.rcParams["legend.frameon"] = True
plt.rcParams["legend.title_fontsize"] = 20
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.labelsize"] = 20
plt.rcParams["text.usetex"] = False
plt.rcParams["xtick.labelsize"] = 20
plt.rcParams["ytick.labelsize"] = 20
plt.rcParams["legend.framealpha"] = 1

plt.rcParams["axes.titlesize"] = 22
plt.rcParams["axes.titleweight"] = "bold"


def back_to_dict(dp):
    outer_keys = dp.keys()
    outer_dict = {k: dict() for k in dp.keys()}
    for okey in outer_keys:
        for key, val in dp[okey].items():
            outer_dict[okey][key] = val
    out_pds = {k: None for k in dp.keys()}
    for okey in outer_keys:
        out_pds[okey] = pd.DataFrame(outer_dict[okey], index=[0])
    return outer_dict, out_pds


def get_beta_star(tdfs, ips=["ip1"]):
    n = len(tdfs)
    nips = len(ips)
    betasx = np.zeros((nips, n))
    betasy = np.zeros((nips, n))
    for i, ip in enumerate(ips):
        for j in range(n):
            selected_ip = tdfs[j]["name"].str.contains(f"{ip}:1")
            betasx[i, j] = tdfs[j][selected_ip].betx.iloc[0]
            betasy[i, j] = tdfs[j][selected_ip].bety.iloc[0]
    return betasx, betasy


def plot_var(var, steps, tb, irs=[1]):
    n = len(steps)
    # if len(irs) == 1:
    fig, axs = plt.subplots()
    for i in range(n):
        if var in tb[i].keys():
            axs.plot(steps[i], tb[i][var], "-o")
    axs.set_title(f"{var}")
    axs.set_xlabel("Steps")
    fig.show()
    # elif len(irs) == 2:
    #     pass
    # fig, axs = plt.subplots(2, 1)
    # for i in range(len(irs)):
    #     for j in range(n):
    #         if var in tb[j].keys():
    #             axs[i].plot(steps[j], tb[j][var], "-o")
    # axs.set_title(f"{var}")
    # axs.set_xlabel("Steps")
    # fig.show()
    return fig, axs


def plot_grid(steps, bs, betas=None, var_nanes=["", "", "", ""], interpolate=False):
    vv = interp_vars(bs, var_nanes)

    fig, axs = plt.subplots(2, 2, figsize=(13, 8))
    axs = axs.ravel()
    axs[0].set_title(var_nanes[0], loc="left")
    axs[0].plot(steps, vv[0][0], "o", label="Optics")

    axs[1].set_title(var_nanes[1], loc="left")
    axs[1].plot(steps, vv[0][1], "o", label="Optics")

    axs[2].set_title(var_nanes[2], loc="left")
    axs[2].plot(steps, vv[0][2], "o", label="Optics")

    axs[3].set_title(var_nanes[3], loc="left")
    axs[3].plot(steps, vv[0][3], "o", label="Optics")

    if interpolate:
        for i in range(4):
            axs[i].plot(vv[1], vv[2][i], label="Interpolation")
            axs[i].grid(True)

    if betas is not None:
        for ax in axs:
            ax2 = ax.twiny()
            ax2.set_xticks(steps)
            ax2.set_xbound(ax.get_xbound())
            ax2.set_xticklabels(np.round(betas[0], decimals=2), minor=False)
            ax2.set_xlabel(r"$\beta^{\star}$ [m]")
    fig.tight_layout()
    return fig, axs


def get_var(bs, name):
    n = len(bs)
    arr = np.zeros(n)
    for i in range(n):
        arr[i] = bs[i][name][0]
    return arr


def interp_vars(tb, varnames=[]):
    n = len(tb)
    nvars = len(varnames)
    # nvars = 4 # test number for now
    isteps = np.arange(0, n, 1)

    ys = np.zeros((nvars, n))

    for i in range(n):
        for j, var in enumerate(varnames):
            ys[j, i] = tb[i][var][0]
    xs = np.linspace(0, n)
    ys_interp = np.zeros((nvars, xs.size))
    for j in range(nvars):
        ys_interp[j, :] = np.interp(xs, isteps, ys[j])
    return ys, xs, ys_interp
