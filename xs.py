import table
from multiprocessing import Pool, Manager
import os
import numpy as np
import pandas as pd
from cpymad import libmadx
from cpymad.madx import Madx
import matplotlib.pyplot as plt

import xtrack as xt
import xpart as xp
import xobjects as xo

from tools import *


class madpool:
    _common_header = """
        option,-echo,-info;
        system,"mkdir temp";
        call,file="acc-models-lhc/lhc.seq";
        call,file="acc-models-lhc/hllhc_sequence.madx";
        call,file="acc-models-lhc/toolkit/macro.madx";
        exec,mk_beam(7000);
        """

    def __init__(self, n=1, commands=None):
        self.libmad = libmadx.start()
        self.pool_size = n
        if commands is not None:
            # assert len(commands) == n
            self.commands = {key: commands[key] for key in range(self.pool_size)}
        else:
            self.commands = {key: "" for key in range(self.pool_size)}
        self.twisss = [None] * self.pool_size
        self.twiss2 = None
        self.table = None

    def start(self, func=None):
        if func is None:
            func = self.f
        manager = Manager()
        twiss_pdict = manager.list([manager.dict() for _ in range(self.pool_size)])
        table_pdict = manager.list([manager.dict() for _ in range(self.pool_size)])

        table_pdict2 = manager.list([manager.dict() for _ in range(self.pool_size)])
        twiss_xsuite = manager.list([pd.DataFrame for _ in range(self.pool_size)])
        line_vars = manager.list([manager.dict() for _ in range(self.pool_size)])
        for i in range(self.pool_size):
            for irn in table.table_names_all.keys():
                table_pdict2[i][irn] = manager.dict()
        coms = [
            {
                "lib": self.libmad,
                "header": self._common_header,
                "id": index,
                "commands": self.commands[index],
                "twiss": twiss_pdict,
                "t1": table_pdict,
                "t2": table_pdict2,
                "txs": twiss_xsuite,
                "line_vars": line_vars,
            }
            for index in range(self.pool_size)
        ]
        with Pool(processes=self.pool_size) as p:
            p.map(func, coms)

        self.twiss2 = twiss_pdict
        self.table = table_pdict
        self.table2 = table_pdict2
        self.twiss_xsuite = twiss_xsuite
        self.line_vars = line_vars

    def f(self, mem):
        process_id = mem["id"]
        mad = Madx(libmadx=mem["lib"], stdout=False)
        cwd = os.getcwd()
        mad.chdir(f"{cwd}/..")
        mad.input(f"""{mem['header']}""")
        mad.input(mem["commands"])
        for name, tablen in table.table_names_all.items():
            for tn in tablen:
                mem["t1"][process_id][tn] = mad.globals[tn]

        for name, tablen in table.table_names_all.items():
            for tn in tablen:
                mem["t2"][process_id][name][tn] = mad.globals[tn]
        # NOTE: don't use dframe()
        # '+++ memory access outside program range, fatal +++'
        # TODO: save twiss for both beams
        b1_twiss = mad.table.twiss
        for cn in b1_twiss.col_names():
            mem["twiss"][process_id][cn] = b1_twiss[cn]

        line = xt.Line.from_madx_sequence(
            mad.sequence["lhcb1"], deferred_expressions=True
        )
        line.particle_ref = xp.Particles(
            p0c=7000e9, q0=1, mass0=xp.PROTON_MASS_EV  # eV
        )
        context = xo.ContextCpu()
        line.build_tracker(_context=context)

        tw = line.twiss(method='4d')
        for name, tablen in table.table_names_all.items():
            for tn in tablen:
                mem["line_vars"][process_id][tn] = line.vars[tn].__value
        mem["txs"][process_id] = tw.to_pandas()


if __name__ == "__main__":
    str = """
        option,-echo,-info;
        system,"mkdir temp";
        call,file="acc-models-lhc/lhc.seq";
        call,file="acc-models-lhc/hllhc_sequence.madx";
        call,file="acc-models-lhc/toolkit/macro.madx";
        exec,mk_beam(7000);
        l.mbh = 0.001000;
        !ACSCA, HARMON := HRF400;
        """

    commands = [
        """
        call,file="acc-models-lhc/strengths/round/start_collapse/opt_collapse_3050_1500.madx";
        exec, myslice();
        exec, check_ip(b1);
        """,
        """
        call,file="acc-models-lhc/strengths/round/start_collapse/opt_collapse_2960_1500.madx";
        exec, myslice();
        exec, check_ip(b1);
        """,
        """
        call,file="acc-models-lhc/strengths/round/levelling/opt_levelling_590_1500.madx";
        exec, myslice();
        exec, check_ip(b1);
        """,
        """
        call,file="acc-models-lhc/strengths/round/levelling/opt_levelling_580_1500.madx";
        exec, myslice();
        exec, check_ip(b1);
        """,
    ]

    # commands = [
    #     """
    #     call,file="acc-models-lhc/strengths/round/start_collapse/opt_collapse_3050_1500.madx";
    #     exec, myslice();
    #     exec, check_ip(b1);
    #     """,
    #     """
    #     call,file="acc-models-lhc/strengths/round/start_collapse/opt_collapse_2960_1500.madx";
    #     exec, myslice();
    #     exec, check_ip(b1);
    #     """,
    #     """
    #     call,file="acc-models-lhc/strengths/round/start_collapse/opt_collapse_2350_1500.madx";
    #     exec, myslice();
    #     exec, check_ip(b1);
    #     """,
    # ]
    # nn = 1
    nn = len(commands)
    mp = madpool(nn, commands)
    mp._common_header = str
    mp.start()

    twiss_dfs = [None] * nn
    for i in range(nn):
        twiss_dfs[i] = pd.DataFrame(dict(mp.twiss2[i]))

    bs = [None] * nn
    for i in range(nn):
        bs[i] = pd.DataFrame(dict(mp.table[i]), index=[0])
    isteps = np.arange(0, nn, 1)

    betasx, betasy = get_beta_star(twiss_dfs, ips=["ip1"])
    plot_grid(
        isteps,
        bs,
        betasx,
        ["kqx1.r1", "kqx2a.r1", "kqx2b.r1", "kqx3.r1"],
        interpolate=True,
    )
    plot_grid(
        isteps,
        bs,
        betasx,
        ["kqx1.l1", "kqx2a.l1", "kqx2b.l1", "kqx3.l1"],
        interpolate=True,
    )
    plot_grid(
        isteps,
        bs,
        betasx,
        ["kq4.l1b1", "kq4.r1b1", "kq5.l1b1", "kq5.r1b1"],
        interpolate=True,
    )
    plt.draw()

    twiss_xs = [None] * nn
    for i in range(nn):
        twiss_xs[i] = pd.DataFrame(dict(mp.twiss_xsuite[i]))
    bs2 = [None] * nn
    for i in range(nn):
        bs2[i] = pd.DataFrame(dict(mp.line_vars[i]), index=[0])
    plot_grid(
        isteps,
        bs2,
        betasx,
        ["kq4.l1b1", "kq4.r1b1", "kq5.l1b1", "kq5.r1b1"],
        interpolate=True,
    )
    plot_grid(
        isteps,
        bs2,
        betasx,
        ["kqx1.r1", "kqx2a.r1", "kqx2b.r1", "kqx3.r1"],
        interpolate=True,
    )
    plot_grid(
        isteps,
        bs2,
        betasx,
        ["kqx1.l1", "kqx2a.l1", "kqx2b.l1", "kqx3.l1"],
        interpolate=True,
    )
    plt.draw()
    plt.show()
